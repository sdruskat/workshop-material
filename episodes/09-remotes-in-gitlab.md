<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Remotes in GitLab

This episode is based on the original *The Carpentries* episodes [Remotes in GitHub](https://swcarpentry.github.io/git-novice/07-github/index.html) and [Collaborating](https://swcarpentry.github.io/git-novice/08-collab/index.html).
In contrast, we use GitLab in this derived episode.
Please see the original episodes for further details about the introduced topics. 

## Repositories

- There are different options and use cases for multiple repositories as shown in: [Git-SCM.com/about/distributed](https://git-scm.com/about/distributed)
- Explain that we are using a `shared repository` which is used for synchronizing the work of all involved persons.

## Publish the local Repository in GitLab

- Create an **empty** GitLab project named `planets` and visibility `private` via the [GitLab Web interface](https://gitlab.com/help/gitlab-basics/create-project.md):
  - Explain available options & usefulness of description for [`/explore` filter](https://gitlab.com/explore/projects)
  - Ask, what happens when the create button is clicked ([`git init --bare`](https://swcarpentry.github.io/git-novice/fig/git-freshly-made-github-repo.svg)).
    - `Initialize ... README` would cause "Initial commit" (this option should be unchecked!)
- Explain the concept of **Remotes**
  - Preview [`clone`, `fetch` vs. `pull`, `push` commands][remote-overview]
  - Copy URL from newly created repository and explicitly mention to copy the URL via the clipboard button ("invisible" character issues)
  - **Challenge:** Which of the `Command line instructions` applies to us? **Answer:** "Push ... existing ..." 
  - Explain the naming convention for `origin` ("origin" from which everyone starts)
  - Set the remote: `git remote add origin URL`
  - Show that a new remote was added: `git remote -v` (explain `fetch` and `push`)
- Push the recent changes to the new repository:
  - `git push`
  - Explain the error message and fix it via the `--set-upstream` hint
- Show the result in GitLab:
  - Explain usefulness of good commit messages for `History` and `Blame` views
- **Exercise:** [Push vs. Commit](https://swcarpentry.github.io/git-novice/07-github/index.html#push-vs-commit)

## Change the remote Repository and synchronize locally

- Create the file `venus.txt` via the GitLab Web interface:
  - Add line: `Wolfman will appreciate the absence of moons`
  - Set `Commit message` to `"Start notes on Venus as a base"`
  - Ask, what might happen if the commit button is clicked
- Change the file `mars.txt` via the GitLab Web interface:
  - Add line: `We have to bring our own oxygen`
  - Set `Commit message` to `"Add notes about Mars' atmosphere"`
- Fetch the changes
  - `git fetch`
  - `git log --oneline --all` and compare with the `Graph` in GitLab
  - `git status` => "behind"
- Integrate the changes into the local `master` branch
  - `git pull` 
  - `git status` => "up to date with 'origin/master'"
  - Highlight difference to **working copies** in SVN
  - Explain that all changes were included **without** merge commit ("fast-forward")
  - Show [Atlassian's "Fast Forward Merge" figure](https://www.atlassian.com/git/tutorials/using-branches/git-merge)

## Key Points

- Local copy is backup of remote => Different local backup needed!
- Starting projects locally or online makes little difference, but:
  - "Commit early, commit often, push early, push often" is safer.
- Start with HTTPS, learn SSH in case of different remote servers.
- Show [`clone`, `fetch` vs. `pull`, `push` overview][remote-overview] again

[remote-overview]: https://rlogiacco.wordpress.com/2017/04/05/git-tricks/
